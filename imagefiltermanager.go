//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package captchas_with_go

//ImageFilterManager
type ImageFilterManager struct {
	filters []ImageFilter
}

func init() {
	RegisterImageFilter(IMAGE_FILTER_NOISE_LINE, imageFilterNoiseLineCreator)
	RegisterImageFilter(IMAGE_FILTER_NOISE_POINT, imageFilterNoisePointCreator)
	RegisterImageFilter(IMAGE_FILTER_STRIKE, imageFilterStrikeCreator)
}

func CreateImageFilterManager() *ImageFilterManager {
	ret := new(ImageFilterManager)
	ret.filters = []ImageFilter{}
	return ret
}

func (manager *ImageFilterManager) AddFilter(filter ImageFilter) {
	manager.filters = append(manager.filters, filter)
}

func (manager *ImageFilterManager) GetFilters() []ImageFilter {
	return manager.filters
}

func CreateImageFilterManagerByConfig(config *FilterConfig) *ImageFilterManager {
	mgr := new(ImageFilterManager)
	mgr.filters = []ImageFilter{}

	for _, cfgId := range config.Filters {
		creator, has := imageFilterCreators[cfgId]
		if !has {
			continue
		}
		cfgGroup, has := config.GetGroup(cfgId)
		if !has {
			continue
		}
		mgr.AddFilter(creator(cfgGroup))
	}

	return mgr
}
